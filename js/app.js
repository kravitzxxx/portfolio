$(document).foundation();

// AOS
AOS.init();

// SLIDERS
// $('.slider-projects').slick({
//     dots: true,
//     arrows: false,
//     infinite: false,
//     slidesToShow: 2,
//     slidesToScroll: 1,
//     autoplay: true,
//     autoplaySpeed: 2000,
//     responsive: [
//         {
//             breakpoint: 769,
//             settings: {
//                 slidesToShow: 2,
//                 slidesToScroll: 1
//             }
//         },
//         {
//             breakpoint: 640,
//             settings: {
//                 slidesToShow: 1,
//                 slidesToScroll: 1
//             }
//         }
//         // You can unslick at a given breakpoint now by adding:
//         // settings: "unslick"
//         // instead of a settings object
//     ]
// });

// RELLAX
var rellax = new Rellax(".rellax");
// var rellax = new Rellax('.rellax', {
//     speed: -2,
//     center: false,
//     wrapper: null,
//     round: true,
//     vertical: true,
//     horizontal: false
// });

// HEADER AFTER SCROLL
$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    //console.log(scroll);
    if (scroll >= 100) {
        //console.log('a');
        $("header").addClass("scrolled");
    } else {
        //console.log('a');
        $("header").removeClass("scrolled");
    }
});