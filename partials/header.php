<header data-magellan>
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="small-2 cell">
                <a href="#intro" class="logo"><img src="images/logo-new.svg" alt=""></a><!-- end logo -->
            </div><!-- end small-2 cell -->
            <div class="small-10 cell">
                <ul>
                    <li><a href="#intro">About</a></li>
                    <li><a href="#projects">Projects</a></li>
                    <li class="show-for-medium"><a href="#get-in-touch" class="btn">Get in touch</a></li>
                    <li class="show-for-small-only"><a href="#get-in-touch" class="btn"><span>&nbsp;</span></a></li>
                </ul>
            </div><!-- end small-10 cell -->
        </div><!-- end grid-x grid-padding-x -->
    </div><!-- end grid-container -->
</header>