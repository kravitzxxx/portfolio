<section class="intro" id="intro">
    <div class="align-content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="small-12 medium-9 cell">
                    <h1><strong>Hi, you found me!</strong></h1>
                    <h2>Currently I'm a freelance designer and web developer, who works with a variety of clients and on many diverse projects.</h2>
                    <h4>Levente Balogh</h4>
                </div><!-- end small-12 medium-9 cell -->
            </div><!-- end grid-x grid-padding-x -->
        </div><!-- end grid-container -->
    </div><!-- end align-content -->
</section>
<div class="grid-container">
    <div class="grid-x grid-padding-x">
        <div class="small-12 medium-4 medium-offset-8 large-3 large-offset-9 cell">
            <div class="rellax" data-rellax-speed="8">
                <ul class="list">
                    <li><h4>Branding</h4></li>
                    <li><h4>logo</h4></li>
                    <li><h4>Interface Design</h4></li>
                    <li><h4>Web Design</h4></li>
                    <li><h4>Web Development</h4></li>
                    <li><h4>Wordpress</h4></li>
                    <li><h4>Illustration</h4></li>
                    <li><h4>Asset Animation</h4></li>
                </ul>
            </div><!-- end rellax -->
        </div><!-- end small-12 large-3 large-offset-9 cell -->
    </div><!-- end grid-x grid-padding-x -->
</div><!-- end grid-container -->