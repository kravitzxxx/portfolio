<footer>
    <div class="grid-container">
    <div class="grid-x grid-padding-x">
        <div class="small-12 cell">
        <p>&copy; <?php echo date('Y'); ?> Kravitz Design.</p>
        </div><!-- end small-12 cell -->
    </div><!-- end grid-x grid-padding-x -->
    </div><!-- end grid-container -->
</footer>