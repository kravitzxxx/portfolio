<section class="projects" id="projects">
    <div class="align-content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="small-12 medium-8 cell" data-aos="fade-up" data-aos-delay="300">
                    <h2><strong>Projects</strong></h2>
                    <h5>I work to create innovative solutions that inspire, and foster memorable relationships between brands and their clients. With a focus on branding and UI / Web, I strive to create usable and polished products through passionate and deliberate design.</h5>
                </div><!-- end small-12 medium-8 cell -->
            </div><!-- end grid-x grid-padding-x -->
            <div class="grid-x grid-padding-x" data-equalizer data-equalie-on="medium">
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="https://www.home-miotto.ro" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/home-miotto.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="https://www.home-miotto.ro">Home-miotto.ro</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme Development</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="https://bitloop.tech" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/bitloop.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="https://bitloop.tech">Bitloop.tech</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Website Development</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="https://explore36.com" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/explore36.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="https://explore36.com">Explore36.com</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme Development</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="http://www.clinicaefigia.ro/en/" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/clinica_efigia.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="http://www.clinicaefigia.ro/en/">Clinicaefigia.ro</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="http://www.mobila-mures.ro" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/mobila-mures.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="http://www.mobila-mures.ro">Mobila-Mures.ro</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="https://www.univer.ro" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/univer.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="https://www.univer.ro">Univer.ro</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="https://stonefilm.eu" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/stonefilm.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="https://stonefilm.eu">Stonefilm.eu</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="http://kornyi.com" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/kornyi.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="http://kornyi.com">Kornyi.com</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="http://sonnhofhotel.com" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/hotelsonnhof.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="http://sonnhofhotel.com">Sonnhofhotel.com</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="https://cijfertalenten.nl" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/cijfertalenten.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="https://cijfertalenten.nl">Cijfertalenten.nl</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="http://freesport.ro" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/freesport.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="http://freesport.ro">Freesport.ro</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
                <div class="small-12 medium-6 cell">
                    <article data-aos="fade-up" data-aos-delay="300">
                        <a target="_blank" href="https://maren.ro" class="featured-image" data-equalizer-watch>
                            <div>
                                <img src="images/projects/resized/maren.png" alt="">
                            </div>
                            <span>View Website</span>
                        </a><!-- end featured-image -->
                        <ul>
                            <li><strong>Project</strong><a target="_blank" href="https://maren.ro">Maren.ro</a></li>
                            <li><strong>Role</strong></li>
                            <li>Logo design</li>
                            <li>Website Design</li>
                            <li>Wordpress Theme</li>
                        </ul>
                    </article>
                </div><!-- end small-12 medium-6 cell -->
            </div><!-- end grid-x grid-padding-x -->
        </div><!-- end grid-container -->
    </div><!-- end align-content -->
</section>