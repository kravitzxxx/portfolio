<section class="get-in-touch" id="get-in-touch">
    <div class="align-content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="small-12 cell" data-aos="fade-up" data-aos-delay="300">
                    <h4>Let’s work together</h4>
                    <br>
                    <h3>I'm a Designer with a focus on Visual and Interaction Design. I design and craft products and visual experiences based on a deep understanding of customer needs.</h3>
                    <br>
                    <br>
                    <ul>
                        <li><a href="#">Email</a></li>
                        <li><a href="#">LinkedIn</a></li>
                        <li><a href="#">Instagram</a></li>
                        <li><a href="#">Facebook</a></li>
                    </ul>
                </div><!-- end small-12 cell -->
            </div><!-- end grid-x grid-padding-x -->
        </div><!-- end grid-container -->
    </div><!-- end align-content -->
</section>