<div class="slider-projects">
    <div>
        <article>
            <ul>
                <li><strong>Project</strong><a target="_blank" href="https://www.home-miotto.ro">Home-miotto.ro</a></li>
                <li><strong>Role</strong></li>
                <li>Logo design</li>
                <li>Website Design</li>
                <li>Wordpress Custom Theme</li>
            </ul>
            <div class="featured-image">
                <div>
                    <img src="images/home-miotto.png" alt="">
                </div>
            </div><!-- end featured-image -->
        </article>
    </div>
    <div>
        <article>
            <ul>
                <li><strong>Project</strong><a target="_blank" href="https://explore36.com">Explore36.com</a></li>
                <li><strong>Role</strong></li>
                <li>Logo design</li>
                <li>Website Design</li>
                <li>Wordpress Custom Theme</li>
            </ul>
            <div class="featured-image">
                <div>
                    <img src="images/explore36.png" alt="">
                </div>
            </div><!-- end featured-image -->
        </article>
    </div>
    <div>
        <article>
            <ul>
                <li><strong>Project</strong><a target="_blank" href="https://explore36.com">Explore36.com</a></li>
                <li><strong>Role</strong></li>
                <li>Logo design</li>
                <li>Website Design</li>
                <li>Wordpress Custom Theme</li>
            </ul>
            <div class="featured-image">
                <div>
                    <img src="images/explore36.png" alt="">
                </div>
            </div><!-- end featured-image -->
        </article>
    </div>
    <div>
        <article>
            <ul>
                <li><strong>Project</strong><a target="_blank" href="https://explore36.com">Explore36.com</a></li>
                <li><strong>Role</strong></li>
                <li>Logo design</li>
                <li>Website Design</li>
                <li>Wordpress Custom Theme</li>
            </ul>
            <div class="featured-image">
                <div>
                    <img src="images/explore36.png" alt="">
                </div>
            </div><!-- end featured-image -->
        </article>
    </div>
</div><!-- end slider-projects -->