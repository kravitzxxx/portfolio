<?php include_once('partials/head.php'); ?>
    
<?php include_once('partials/header.php'); ?>    
<?php include_once('partials/intro.php'); ?>
<?php include_once('partials/projects.php'); ?>
<?php include_once('partials/get-in-touch.php'); ?>

<?php include_once('partials/footer.php'); ?>    

<?php include_once('partials/scripts.php'); ?>